class Factory {

    tableName;
    params;

    withTableName(tableName) {
        this.tableName = tableName;
        return this;
    }

    withParams(params) {
        this.params = params;
        return this;
    }

    build() {
        const params = {
            TableName: this.tableName,
            FilterExpression: ":baseExpression = :baseExpression",
            ExpressionAttributeNames: {},
            ExpressionAttributeValues: {":baseExpression": 1}
        };

        Object.keys(this.params).forEach(key => {
            let fieldPlacehodler = "#" + key;
            let valuePlaceholder = ":" + key + "Filter";

            params.ExpressionAttributeNames[fieldPlacehodler] = key;
            params.ExpressionAttributeValues[valuePlaceholder] = this.params[key];
            params.FilterExpression += ` and ${fieldPlacehodler} = ${value}`;
        })

        return params;
    }

}
